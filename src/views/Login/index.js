import React, { Component } from "react"
import LoginForm from "../../components/Login/LoginForm"

class Login extends Component {

    render() {
        return (
            <LoginForm isRegister = {this.props.location.pathname === '/register'}/>
        )
    }
}

export default Login