import React, { Component } from "react";
import { connect } from "react-redux";
import GameList from '../../components/Game/GameList.js';
import { action } from './GameRedux.js';
import LoadingBox from "../../hoc/LoadingBox"

const WinnerListWithLoading = LoadingBox(GameList);
class Game extends Component {

    constructor(props){
        super(props);
        this.props.loadAllGameData()
    }
    render() {

        const { isLoading, loadAllGame} = this.props;
        console.log(isLoading)

        return <WinnerListWithLoading isloading={isLoading} loadAllGame={loadAllGame} />
    }
}

export default connect(
    (state) => ({list: state.game.list.gameList, isLoading: state.game.list.isLoading }),
    {...action}
)(Game);
