import * as actions from '../../components/Game/GameListRedux.js';
import list from '../../components/Game/GameListRedux.js';
import { combineReducers } from 'redux';

export default combineReducers({list});

export const action = { ...actions};