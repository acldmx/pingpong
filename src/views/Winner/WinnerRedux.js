import { combineReducers } from "redux"
import list, { loadWinners } from "../../components/Winner/WinnerListRedux"

export default combineReducers({
    list,
})

export const actions = {
    loadWinners
}