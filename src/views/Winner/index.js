import React, { Component } from 'react';
import { Layout, Input } from 'antd'
import WinnerList from '../../components/Winner/WinnerList'
import { loadWinners } from "../../components/Winner/WinnerListRedux"
import LoadingBox from "../../hoc/LoadingBox"
import { connect } from "react-redux";

const { Search } = Input

const WinnerListWithLoading = LoadingBox(WinnerList)

class Winner extends Component {
    constructor(props) {
        super(props)
        this.props.loadWinners()
    }

    render() {
        const { isListLoading } = this.props
        return (
            <Layout>
                <Search
                    style={{marginTop: '1rem'}}
                    placeholder="请输入需要查找的冠军"
                    enterButton
                    size="large"
                    onSearch={this.props.loadWinners}
                />
                <WinnerListWithLoading
                    isloading={isListLoading}
                    info={this.props.list}
                />
            </Layout>
        )
    }
}

export default connect(
    state => {
        return {
            list: state.winner.list.winners,
            isListLoading: state.winner.list.isloading,
        }
    },
    {
        loadWinners
    }
)(Winner)