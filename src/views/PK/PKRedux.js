import { combineReducers } from "redux"
import info, { loadPlayersInfo } from "../../components/PK/PKInfoListRedux"
import history, { loadPlayersHistory } from "../../components/PK/PkPlayerHistoryRedux"
import input, { setName } from "../../components/PK/PKInputRedux"

export default combineReducers({
    info,
    history,
    input
})

export const actions = {
    loadPlayersInfo,
    loadPlayersHistory,
    setName,
}