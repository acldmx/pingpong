import React, { Component } from 'react';
import { connect } from "react-redux"
import PKInput from "../../components/PK/PKInput"
import PKInfoList from "../../components/PK/PKInfoList"
import PKPlayerHistory from "../../components/PK/PKPlayerHistory"
import { actions } from "../PK/PKRedux"
import style from "./pk.module.css"
import LoadingBox from "../../hoc/LoadingBox"

/**
 * 带有加载效果的高阶组件
 */
const PKInfoListWithLoading = LoadingBox(PKInfoList)
const PKPlayerHistoryWithLoading = LoadingBox(PKPlayerHistory)

const { loadPlayersInfo, loadPlayersHistory, setName } = actions

const mainPage = ({ infoLoading, historyLoading, playersInfo, playersHistory }) => (
    <div>
        <h2 className={style.header}>
            个人信息
                </h2>
        <PKInfoListWithLoading
            isloading={!!infoLoading}
            playersInfo={playersInfo}
        />

        <h2 className={style.header}>
            比赛详细
                </h2>
        <PKPlayerHistoryWithLoading
            isloading={!!historyLoading}
            data={playersHistory} />
    </div>
)

const errorHandlePage = props => {
    const { error } = props

    return (
        <h1>{
            !!error ? "未查找到用户" : "请输入需要查找的用户"
        }</h1>
    )
}


class PK extends Component {

    constructor(props) {
        super(props)
        this.handleSearchDo = this.handleSearchDo.bind(this)
    }


    handleSearchDo(homeName, awayName) {
        //拿到定义的 redux action
        const { loadPlayersInfo, loadPlayersHistory, setName } = this.props
        //发送网络请求
        loadPlayersInfo(homeName, awayName)
        loadPlayersHistory(homeName, awayName)
        setName("homeName", homeName)
        setName("awayName", awayName)
    }

    render() {
        const { homeName, awayName, error, hasSearch } = this.props //connect中拿出来的状态

        const childs = hasSearch && !error ? mainPage(this.props) : errorHandlePage(this.props)

        return (
            <div>
                <h2 className={style.header}>球员对比</h2>
                <PKInput
                    onClick={this.handleSearchDo}
                    homeName={homeName}
                    awayName={awayName}
                />
                {childs}
            </div>
        )
    }
}


export default connect(state => {
    return {
        homeName: state.pk.input.homeName,
        awayName: state.pk.input.awayName,
        playersInfo: state.pk.info.players,
        infoLoading: state.pk.info.loading,
        hasSearch: state.pk.info.hasSearch,
        error: state.pk.info.error,
        playersHistory: state.pk.history.players,
        historyLoading: state.pk.history.loading,
    }
}, {
        loadPlayersInfo,
        loadPlayersHistory,
        setName
    })(PK)