import list from "../../components/Home/PongListRedux";
import history from "../../components/Home/HistoryModalRedux";
import { combineReducers } from "redux";
import * as dataAction from "../../components/Home/PongListRedux";
import * as historyAction from "../../components/Home/HistoryModalRedux";

export default combineReducers({
  list,
  history,
});

export const actions = {
  ...dataAction,
  ...historyAction
}
