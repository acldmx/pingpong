import React, { Component } from "react";
import { connect } from "react-redux";
import { Input } from "antd"
import { actions } from "./HomeRedux";
import PongList from "../../components/Home/PongList";
import HistoryModal from "../../components/Home/HistoryModal"
import style from "./Home.module.css"

const { Search } = Input;

class Home extends Component {
  constructor(props) {
    super(props);

    this.handleModal = this.handleModal.bind(this);
    this.handleModelClose = this.handleModelClose.bind(this);
    this.state = {
      visibleModal: false,
    }
  }


  handleModal(item) {
    const { name } = item
    this.setState({
      visibleModal: true,
    })
    this.props.loadPlayerHistory(name);
  }

  handleModelClose() {
    this.setState({
      visibleModal: false,
    })
  }

  render() {
    const { loadAllData, players, isLoading } = this.props;
    return (
      <div>
        <Search
          className={style.search}
          placeholder="请输入需要查找的名称"
          enterButton
          size="large"
          onSearch={loadAllData}
        />

        <PongList
          list={players}
          loadAllData={loadAllData}
          isLoading={isLoading}
          onItemClick={this.handleModal}
        />

        <HistoryModal
          visible={this.state.visibleModal}
          onCancel={this.handleModelClose}
          info={this.props.playerHistory}
        />
      </div>
    );
  }
}

export default connect(

  state => {
    return {
      players: state.home.list.players, 
      isLoading: state.home.list.isLoading,
      playerHistory: state.home.history.player,
    };
  },
  {
    ...actions,
  }
)(Home);
