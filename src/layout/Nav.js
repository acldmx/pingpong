import React from 'react'
import { Menu } from 'antd'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { Icon } from 'antd'
const { Item } = Menu;

class Nav extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            current: '/',
        }

        this.modidyNav = this.modidyNav.bind(this)
    }

    // 保证在当前页面刷新nav保持selected
    handleClick = e => {
        this.modidyNav(e);
    };

    modidyNav(e){
        this.setState({
            current: e? e.key : this.props.location.pathname
        })
    }

    componentDidMount(){
        this.modidyNav();
    }

    render(){
        return (
            <Menu
                onClick={this.handleClick}
                selectedKeys={[this.state.current]}
                mode="horizontal"
                style={{ lineHeight: '64px' }}>
                <Item key='/'>
                    <Link to="/">
                        积分榜
                    </Link>
                </Item>
                <Item key='/winner-list'>
                    <Link to="/winner-list">
                        冠军榜
                    </Link>
                </Item>
                <Item key='/pk'>
                    <Link to="/pk">
                        PK榜
                    </Link>
                </Item>
                <Item key={['/manage']}>
                    <Link to="/manage">
                        管理
                    </Link>
                </Item>
                <Item key='/game'>
                    <Link to="/game">
                        比赛
                    </Link>
                </Item>
            </Menu>
        )
    }
}
export default  withRouter(Nav);