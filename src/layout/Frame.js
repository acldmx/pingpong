import React from 'react';
import { Layout } from 'antd'
import Nav from "./Nav" 
const { Header, Content } = Layout

export default function Frame(props) {
    return (
        <Layout>
            <Header style={{marginBottom: "20px"}}>
                <Nav />
            </Header>
            <Content>
                {props.children}
            </Content>
        </Layout>
    )
}