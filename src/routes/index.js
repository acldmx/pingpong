import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Frame from "../layout/Frame"
import Home from "../views/Home"
import Winner from "../views/Winner"
import PK from "../views/PK"
import Login from "../views/Login"
import Game from "../views/Game"
import AuthorizedRoute from './AuthorizedRoute.js'
import Manage from '../views/Manage'

export default (
    <Router>
        <Frame>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/winner-list" component={Winner} />
                <Route path="/pk" component={PK} />
                <Route path={["/login", "/register"]} component={Login} />
                <Route path="/game" component={Game} />
                <AuthorizedRoute path="/manage" component={Manage}/>
            </Switch>
        </Frame>
    </Router>
)