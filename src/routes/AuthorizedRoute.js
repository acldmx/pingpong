import React from 'react'
import { Route, Redirect } from 'react-router-dom'
export default class AuthorizedRoute extends React.Component {
    render() {
        const { component: Component, ...rest } = this.props
        const isLogged = localStorage.getItem("token") != null
        
        return (
                <Route {...rest} render={props => {
                    return isLogged
                            ?  <Component {...props} />
                            : <Redirect to="/login" />
                }} />
        )
    }
}
