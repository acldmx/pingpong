import { createStore, applyMiddleware } from "redux";
import createFetchMiddleware from "redux-composable-fetch"
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from "./reducer";

const FetchMiddleware = createFetchMiddleware()
const composeEnhancers = composeWithDevTools({})

/**
 * 解析带有response对象的中间件
 */
const responseMiddleware = ({ dispatch }) => next => action => {
  if (!action || !action.payload || !(action.payload instanceof Response)) {
    return next(action)
  }
  
  const { payload } = action
  return payload.json().then(d => {
    next({
      ...action,
      payload: d,
    })
  })
}

//多异步串联
const sequenceMiddleware = ({ dispatch }) => next => action => {
  //判断是否是数组的action
  if (!Array.isArray(action)) {
    return next(action)
  }

  return action.reduce((result, currAction) => {
    return result.then(() => {

      //action => [[]]
      if (Array.isArray(currAction)) {
        return Promise.all(currAction.map(item => dispatch(item)))
      } else { 
        
        //action => []
        return dispatch(currAction)
      }
    })
  }, Promise.resolve())
}

export default function initStore() {
  return createStore(reducer,
    composeEnhancers(
      applyMiddleware(
        sequenceMiddleware,
        FetchMiddleware,
        responseMiddleware
      )
    )
  );
}
