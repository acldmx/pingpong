import { combineReducers } from "redux";
import home from "../views/Home/HomeRedux";
import pk from "../views/PK/PKRedux";
import winner from "../views/Winner/WinnerRedux";
import common from "../components/shared/Redux";
import game from "../views/Game/GameRedux.js";
export default combineReducers({
  home,
  winner,
  pk,
  common,
  game
});
  