import React, { Component } from "react"
import { Table } from 'antd'

class WinnerList extends Component {
    static columns = [
        {
            title: '比赛',
            dataIndex: 'game',
            key: 'game',
        },
        {
            title: '冠军',
            dataIndex: 'name',
            key: 'name',
        },
    ];

    render() {
        const { info } = this.props
        return <Table
            rowKey="id"
            columns={WinnerList.columns}
            dataSource={info}
            pagination={false} />
    }
}

export default WinnerList;