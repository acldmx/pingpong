import React, { Component } from "react"
import { Form, Input, Button, Checkbox } from "antd"
import { Link } from "react-router-dom"
import MsgBox from '../shared/MsgBox.js'

const { Item } = Form

class LoginForm extends Component {

    constructor(props){
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    compareToFirstPassword = (rule, value, callback) => {
        const {form} = this.props
        if (value && value !== form.getFieldValue('password')) {
            callback('两次密码不一致')
        } else {
            callback()
        }
    }

    handleSubmit = e => {
        e.preventDefault()
        //注册逻辑
        if (this.props.isRegister) {
            
        //登录逻辑
        } else {
            this.handleLogin();
        }
    }

    handleLogin = e => {
        MsgBox('success')('success');
    }

    handleRegister = e => {

    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { isRegister } = this.props
        return (
            <Form>
                <Item>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: '请输入用户名' }]
                    })(<Input placeholder="请输入用户名" size="large" />)}
                </Item>
                <Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: "请输入密码" }]
                    })(<Input.Password placeholder="请输入密码" size="large" type="password" />)}
                </Item>
                {
                    isRegister ?
                        <Item>
                            {getFieldDecorator('confrim', {
                                rules: [{ required: true, message: "请再次输入密码" }, {
                                    validator: this.compareToFirstPassword,
                                }]
                            })(<Input.Password placeholder="请再次输入密码" size="large" type="password" />)}
                        </Item> :
                        null
                }
                <Item>
                    {
                        !isRegister ?
                            <Checkbox>Remember me</Checkbox> :
                            null
                    }
                </Item>
                <Item>
                    <Button onClick={this.handleSubmit} type="primary" block>
                        {!isRegister ? "注册" : "登录"}
                    </Button>
                </Item>
                <Item>
                    {
                        isRegister ?
                            <Link to="/login">去登录</Link> :
                            <Link to="/register">去注册</Link>
                    }

                </Item>
            </Form>
        )
    }
}

const WrappedLoginForm = Form.create({ name: "login_form" })(LoginForm)
export default WrappedLoginForm