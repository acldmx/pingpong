const LOAD_GAME_DATA = "load_game_data";
const LOAD_GAME_DATA_SUCCESS = "load_game_data_success";
const LOAD_GAME_DATA_ERROR = "load_game_data_error";


export function loadAllGameData(queryName = "") {
    return {
      types: [LOAD_GAME_DATA, LOAD_GAME_DATA_SUCCESS, LOAD_GAME_DATA_ERROR],
      url: `http://localhost:8080/winner-list?name=${queryName}`,
    };
}

const initState = {
    gameList: [],
    isLoading: false,
}


  /*
  function loadAllGame(state, action) {
    return {
      ...state,
      isLoading: true, 
    };
  }
  
  function loadAllGameSuccess(state, action) {
    const { gameList } = action.payload; 

    //应该是时间顺序进行排序 这里需要排序
    const newList = gameList.sort();
    return {
      ...state,
      isLoading: false,
      gameList: newList,
    }
  }
*/
  export default function reducer(state = initState, action){

   //网络请求的结果全部都在payLoad中
   const { type, payload } = action
    switch (type) {
        case LOAD_GAME_DATA:
            return {
                ...state,
                gameList: [],
                isLoading: true,
            }
        case LOAD_GAME_DATA_SUCCESS:
            return {
                ...state,
                gameList: payload,
                isLoading: false,
            }
       
        case LOAD_GAME_DATA_ERROR:
            return {
              ...state,
              isLoading: false,
            }    
        default:
          return state
    }
  }