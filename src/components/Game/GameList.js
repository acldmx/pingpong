import React, { Component } from "react";
import PropTypes from "prop-types";
import { Table } from "antd";

class GameList extends Component {
  static propTypes = {
    gameList: PropTypes.array
  };

  static defaultProps = {
    gameList: []
  };

  static COLUMNS = [
    {
      title: "比赛名称",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "比赛时间",
      dataIndex: "time",
      key: "time"
    },
    {
      title: "赞助商",
      dataIndex: "sponsor",
      key: "sponsor",
    },
    {
      title: "比赛规则",
      dataIndex: "rules",
      key: "rules",
    },
    {
        title: '比赛状态',
        dataIndex: 'status',
        key: 'status'
    }
  ];

  

  render() {
    const { gameList} = this.props;
    console.log(gameList)

    return <Table
      rowKey="id"
      columns={GameList.COLUMNS}
      column={{align: "center"}}
      dataSource={gameList}
      pagination={{pageSize: 20}}
     />
  } 
}

export default GameList;
