import React, { Component } from "react";
import PropTypes from "prop-types";
import { Table } from "antd";

class PongList extends Component {
  static propTypes = {
    list: PropTypes.array
  };

  static defaultProps = {
    list: []
  };

  static COLUMNS = [
    {
      title: "排名",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "姓名",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "积分",
      dataIndex: "score",
      key: "score",
    },
    {
      title: "比赛次数",
      dataIndex: "game_count",
      key: "game_count",
    },
  ];

  componentDidMount() {
    this.props.loadAllData();
  }

  render() {
    const { list, isLoading, onItemClick } = this.props;
    const handleItemClick = record => {
      return {
        onClick: event => {
          onItemClick(record)
        }
      }
    }

    return <Table
      onRow={handleItemClick}
      rowKey="id"
      columns={PongList.COLUMNS}
      column={{align: "center"}}
      dataSource={list}
      pagination={{pageSize: 20}}
      loading={isLoading} />
  } 
}

export default PongList;
