//默认初始化状态
const initialState = {
    winnerList: [],
}

//action creators
//获取冠军列表
const LOAD_WINNER_LIST = 'load_winner_list'
const LOAD_WINNER_LIST_SUCCESS = 'load_winner_list_success'
const LOAD_WINNER_LIST_ERROR = 'load_winner_list_error'

export function loadWinnerList() {
    return {
        url: "http://localhost:8080/winner-list",
        types: [LOAD_WINNER_LIST, LOAD_WINNER_LIST_SUCCESS, LOAD_WINNER_LIST_ERROR]
    }
}

//reducer
const handlers = {
    [`${LOAD_WINNER_LIST_SUCCESS}`]: handleLoadWinnerListSuccess,
}

export default function reducer(state = initialState, action) {

    const handler = handlers[action.type]
    if (!!handler) {
        return handler(state, action)
    } else {
        return state
    }
}

//handle reducer
function handleLoadWinnerListSuccess(state, action) {
    const { payload } = action
    console.log(payload)
    
    if (!payload) {
        return state
    }
    
    const winnerListWithId = payload.map((winner, index) => {
        return {
            ...winner,
            name: winner.name.split(","),
            id: index
        }
    })

    return {
        ...state,
        winnerList: winnerListWithId
    }
}
