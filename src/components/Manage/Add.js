import { Form, Input, Button} from 'antd'
import React from 'react'

export default class FormLayoutDemo extends React.Component {
 
  render() {
    
    return (
      <div>
        <Form layout='vertical'>
          <Form.Item label="Field A">
            <Input placeholder="input placeholder" />
          </Form.Item>
          <Form.Item label="Field B">
            <Input placeholder="input placeholder" />
          </Form.Item>
          <Form.Item >
            <Button type="primary">Submit</Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}