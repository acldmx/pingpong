import { Tabs, Select } from 'antd';
import React from 'react'
import {Button} from 'antd'
import Add from './Add.js'
import UploadExcel from './UploadExcel' 

const { TabPane } = Tabs;


export default class ManageTab extends React.Component {
  render() {
    return (
        
        <div style={{margin: '1rem 0 0 -10%'}}>

        
        <Tabs tabPosition='left'>
          <TabPane tab="添加选手" key="1">
            <Add/>
          </TabPane>
          <TabPane tab="上传excel" key="2">
            <UploadExcel/>
          </TabPane>
        </Tabs>
      </div>
    );
  }
}
