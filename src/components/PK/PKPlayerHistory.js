import React from "react"
import { Table } from "antd"

//固定的姓名列
const FIXED_COLUMN = {
    title: "姓名",
    key: "name",
    dataIndex: "name",
    fixed: 'left',
}

//从props中生成列名(比赛为列名)
const convertColumns = (data) => {
    const columnsName = Array.from(
        new Set(
            [].concat(...data).map(item => item.game_name)
        )
    ).filter(f => !!f)

    return [FIXED_COLUMN, ...columnsName.map(name => {
        return {
            title: name,
            key: name,
            dataIndex: name,
        }
    })]
}

const convertRows = d => d.map(player => {
    const name = (player.find(item => item.name) || {name: ""}).name
    return player.reduce((history, item) => {
        history[item.game_name] = `${item.game_count}/${item.score}`
        return history
    }, { name })
})

export default function PKPlayerHistory(props) {
    const { data } = props

    if (!Array.isArray(data) ||
        data.flat().length < 1) {
        return (
            <div></div>
        )
    }

    const columns = convertColumns(data)
    const rows = convertRows(data)

    return (
        <Table
            rowKey="name"
            columns={columns}
            dataSource={rows}
            scroll={{ x: columns.length * 120 }}
            pagination={false}
        />
    )
}