const initialState = {
    players: [],
    loading: 0,
}

const LOAD_PLAYERS_HISTORY = "load_players_history"
const LOAD_PLAYER_HISTORY = "load_player_history"
const LOAD_PLAYER_HISTORY_SUCCESS = "load_player_history_success"
const LOAD_PLAYER_HISTORY_ERROR = "load_player_history_error"

export function loadPlayerHistory(name) {
    return {
        url: `https://pping.goho.co/api/score/history?name=${name}`,
        types: [LOAD_PLAYER_HISTORY, LOAD_PLAYER_HISTORY_SUCCESS, LOAD_PLAYER_HISTORY_ERROR],
    }
}

export function loadPlayersHistory(...names) {
    return [
        {
            type: LOAD_PLAYERS_HISTORY,
        },
        ...names.map(name => loadPlayerHistory(name)),
    ]
}

export default function reducer(state = initialState, action) {
    const { payload, type, meta } = action

    switch (type) {
        case LOAD_PLAYERS_HISTORY:
            return {
                ...state,
                players: [],
                loading: 0,
            }
        case LOAD_PLAYER_HISTORY:
            return {
                ...state,
                loading: state.loading + 1,
            }
        case LOAD_PLAYER_HISTORY_SUCCESS:
            let players = state.players
            return {
                ...state,
                players: [
                    ...players,
                    payload
                ],
                loading: state.loading - 1,
            }
        case LOAD_PLAYER_HISTORY_ERROR:
            return {
                ...state,
                loading: state.loading - 1,
            }
        default:
            return state
    }
}