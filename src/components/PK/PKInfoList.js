import React, { Component } from "react"
import PKInfo from './PKInfo'
export default function PKInfoList(props) {

    const [ homePlayer, awayPlayer ] = props.playersInfo

    if (!homePlayer || !awayPlayer) {
        return (
            <div></div>
        )
    }

    const childs = [{
        title: "姓名",
        key: "name"
    }, {
        title: "比赛次数",
        key: "game_count"
    }, {
        title: "积分",
        key: "score"
    },].map((col, index) => (
        <PKInfo
            title={col.title}
            left={homePlayer[col.key]}
            right={awayPlayer[col.key]}
            key={col.title}
        />
    ))

    return (
        <div>
            {childs}
        </div>
    )
}