const initialState = {
    players: [],
    loading: 0,
    //是否出错
    error: false,
    //是否已搜索
    hasSearch: false,
}

/**
 * 加载两个对比运动员数据
 */
function loadPlayerInfo(name) {
    return {
        url: `http://localhost:8080/player?name=${name}`,
        types: [LOAD_PLAYER_INFO, LOAD_PLAYER_INFO_SUCCESS, LOAD_PLAYER_INFO_ERROR],
        meta: { name }
    }
}

export function loadPlayersInfo(...names) {
    return [{ type: LOAD_PLAYERS_INFO }, ...names.map(name => loadPlayerInfo(name))]
}

const LOAD_PLAYERS_INFO = 'load_players_info'
const LOAD_PLAYER_INFO = 'load_player_info'
const LOAD_PLAYER_INFO_SUCCESS = 'load_player_info_success'
const LOAD_PLAYER_INFO_ERROR = 'load_player_info_error'

export default function reducer(state = initialState, action) {
    const { type, payload, meta } = action

    switch (type) {
        case LOAD_PLAYERS_INFO:
            return {
                ...state,
                players: [],
                hasSearch: true,
                error: false,
            }   
        case LOAD_PLAYER_INFO:
            return {
                ...state,
                loading: state.loading + 1,
            }
        case LOAD_PLAYER_INFO_SUCCESS:
            return {
                ...state,
                players: [
                    ...state.players,
                    payload
                ],
                loading: state.loading - 1,
                error: payload === null ? true: state.error, 
            }
        case LOAD_PLAYER_INFO_ERROR:
            return {
                ...state,
                loading: state.loading - 1,
                error: true,
            }
        default:
            return state
    }
}

