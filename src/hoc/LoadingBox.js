import React, { Component } from 'react'
import { Spin } from 'antd'
function LoadingBox(WrappendComponent) {
    const loading = props => {
        const { isloading, ...rest } = props

        return (
            <Spin spinning={isloading}>
                <WrappendComponent {...rest} />
            </Spin>
        )
    }

    loading.prototype.displayName = `LoadingBox(${getDisplayName(loading.prototype)})`
    return loading
}

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component'
}

export default LoadingBox